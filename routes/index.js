const express = require('express');
const log = require('../libs/log');
const errors = require('./errors');

let app = express();
app.disable('x-powered-by');

app.on('mount', parent => {
    log.info('Routes mounted');
});

app.use('/app/', require('./app'));

app.use((req, res, next) => {
    next(errors.notFound());
});

app.use((err, req, res, next) => {
    if (err instanceof Error)
        err = errors.internalError(err);

    log.error(
        `Error ${(err.status || 'unknown error')} URL: ${req.method} ${req.path}
        \t\t\t\t\t message: ${JSON.stringify(err.description)} 
        \t\t\t\t\t body: ${JSON.stringify(req.body || {}).substring(0, 200)}
        \t\t\t\t\t query: ${JSON.stringify(req.query || {})}`
    );

    err.status = err.status || 500;

    if (err.status === 500) {
        log.debug(`Stacktrace: ${err.error && err.error.stack ? err.error.stack : ''}`);

        if (err.stack)
            delete err.stack;

        if (err.error && err.error.code && err.error.code === 'ERR_OUT_OF_RANGE')
            err = errors.payloadTooLarge('Too large mongoose document');
    }

    res.status(err.status).send(err);
});

module.exports = app;
