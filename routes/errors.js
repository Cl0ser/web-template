'use strict';
const { validationResult } = require('express-validator');

exports.custom = (status, code, description) => {
    return { status, code, description };
};

exports.unauthorized = (description) => {
    return { status: 401, description: description || 'Unauthorized' };
};

exports.forbidden = (description) => {
    return { status: 403, description: description || 'Forbidden' };
};

exports.badRequest = (description) => {
    return { status: 400, description: description || 'Bad request' };
};

exports.notFound = (description) => {
    return { status: 404, description: description || 'Not found' };
};

exports.payloadTooLarge = (description) => {
    return { status: 413, description: description || 'Payload too large' };
};

exports.internalError = error => {
    return { status: 500, description: error.message || 'Internal server error', error };
};

exports.serviceUnavailable = error => {
    return { status: 503, description: error.message || 'Service Unavailable', error };
};


//Throw if errors in validation
exports.handleValidationErrors = (req, res, next) => {
    const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
        if (nestedErrors)
            return nestedErrors.map(errorFormatter).join('\n');

        return `${location}[${param}] = ${value} : ${msg}`;
    };

    const errors = validationResult(req).formatWith(errorFormatter);

    if (!errors.isEmpty()) {
        let errorString = errors.array().reduce((acc, value) => acc + '\n' + value);
        throw { status: 400, description: errorString };
    }

    if (next) // usage like wrapper
        next();
};
