'use strict';

const errors = require('./errors');
const db = require('../db');

module.exports.multipartHandler = (req, fileModel, field, meta) => {
    return new Promise((resolve, reject) => {
        if (!req.busboy) {
            reject(errors.internalError('Busboy not loaded'));
            return;
        }

        let files = [];
        let totalFiles = 0;

        req.busboy.on('field', (fieldname, val) => {
            req.body[fieldname] = val;
        });

        req.busboy.on('file', (fieldName, file, fileName, encoding, mime) => {
            if (fieldName !== field && fieldName !== field + '[]') {
                reject(errors.badRequest(`No file in field ${field}`));
                return;
            }

            ++totalFiles;

            let fileType = db.defines.FILE_TYPE.RAW;

            switch (mime) {
                case 'application/zip':
                case 'application/x-zip-compressed':
                    fileType = db.defines.FILE_TYPE.ZIP;
                    break;
                case 'application/x-tar':
                    fileType = db.defines.FILE_TYPE.TAR;
                    break;
                case 'application/gzip':
                    fileType = db.defines.FILE_TYPE.TAR_GZ;
                    break;
            }

            let fileMeta = { ...meta } || {};

            fileMeta.type = fileType;

            fileModel.pipeStream(fileName, file, fileMeta, mime, (err, data) => {
                if (err) {
                    reject(`Error uploading file ${fileName} error: ${err.message}`);
                }
                else {
                    files.push(data);

                    if (files.length === totalFiles)
                        resolve(files);
                }
            });
        });


        req.pipe(req.busboy);
    });
};

module.exports.attachmentHandler = async (res, gfs, fileId, redirect) => {
    let file = await gfs.fileInfo(fileId);

    if (!file) {
        if (redirect)
            res.redirect(redirect);
        else
            res.status(404).send({});

        return;
    }

    try {
        res.set({ 'Content-Length': file.length, 'File-Id': fileId });
        res.attachment(file.filename);
        res.type(file.contentType);

        await gfs.readStream(file._id, res);
    }
    catch (e) {
        if (e.code === 'ENOENT') {
            if (redirect)
                res.redirect(redirect);
            else
                res.status(404).send({});
        }
        else {
            throw e;
        }
    }
};
