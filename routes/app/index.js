const express = require('express');
const log = require('../../libs/log');

let app = express();
app.disable('x-powered-by');

app.on('mount', parent => {
    log.info('Main app mounted');
});

app.use('/', require('./common'));

module.exports = app;
