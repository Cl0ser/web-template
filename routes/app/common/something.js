const { param, body } = require('express-validator');
const db = require('../../../db');

const errors = require('../../errors');

const router = require('express').Router();

router.post('/createIteration', [
    body('name').isString()
], errors.handleValidationErrors, async (req, res, next) => {
    let iteration = await db.table.add(req.body.name);

    res.send(iteration.toObject());
});

router.get('/allIterations', [], errors.handleValidationErrors, async (req, res, next) => {
    let iterations = await db.table.getAll();

    res.send(iterations);
});


router.get('/:id/increment', [
    param('id').isMongoId()
], errors.handleValidationErrors, async (req, res, next) => {
    await db.table.increment(req.params.id);

    res.end();
});

router.get('/:id/remove', [
    param('id').isMongoId()
], errors.handleValidationErrors, async (req, res, next) => {
    await db.table.remove(req.params.id);

    res.end();
});

module.exports = router;
