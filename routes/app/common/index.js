'use strict';
const router = require('express').Router();

router.use('/something', require('./something'));

module.exports = router;
