'use strict';

const Mogront = require('mogront');
const path = require('path');

module.exports.connect = client => {
    module.exports.client = new Mogront(client, {
        'migrationsDir': path.join(__dirname, 'db', 'migrations'),
        'collectionName': '_migrations'
    });
};

module.exports.client = null;
