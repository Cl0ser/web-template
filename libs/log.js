'use strict';

const winston = require('winston');
const path = require('path');
const cluster = require('cluster');
const config = require('config');

winston.transports.DailyRotateFile = require('winston-daily-rotate-file');
//TODO: create log path before start

module.exports = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.colorize(),
        winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
    ),
    transports: [
        new winston.transports.Console({
            colorize: true,
            level: config.get('log.level'),
            label: cluster.isMaster ? 'Server' : 'Server worker [' + cluster.worker.id + ']'
        }),
        new winston.transports.DailyRotateFile({
            colorize: true,
            level: config.get('log.level'),
            json: false,
            filename: path.resolve(path.join(config.get('log.path'), 'log_file.log'))
        })
    ]
});
