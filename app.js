const config = require('config');

const express = require('express');
require('express-async-errors'); // Handles async errors in route functions by default express error multipartHandler.

const log = require('./libs/log.js');
const db = require('./db');

const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const sessionMiddleware = session({
    proxy: true,
    secret: config.get('secrets.session'),
    resave: false,
    saveUninitialized: true,
    rolling: true,
    name: 'test.sid',
    store: new MongoStore({ mongooseConnection: db.connection })
});

const app = express();

app.use(require('helmet')());
app.disable('x-powered-by');

const bodyParser = require('body-parser');

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use(sessionMiddleware);

app.use(require('connect-busboy')());

app.use(require('./routes'));

app.listen(config.get('port'), async () => {
    log.info(`Server worker ${process.pid} started!`);
});
