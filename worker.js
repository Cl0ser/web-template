'use strict';

const config = require('config');
const db = require('./db');

(async () => {
    let dbStarted = await db.connect(config.get('db.uri'), config.get('db'));

    if (dbStarted)
        require('./app');
})();
