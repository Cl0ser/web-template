'use strict';

const mogront = require('./mogront');
const mongodb = require('mongodb');
const config = require('config');

require('colors');

let client = null;

const _collectionExist = async collectionName => {
    let db = client.db();
    let collections = await db.listCollections().toArray();

    return (collections.indexOf(collectionName) > -1);
};

const maintenance = async value => {
    const CONFIG_COLLECTION = 'configs';

    let db = client.db();

    let exists = await _collectionExist(CONFIG_COLLECTION);
    let collection = (exists ? db.collection(CONFIG_COLLECTION) : await db.createCollection(CONFIG_COLLECTION));

    let maintenance = await collection.findOne({ name: 'maintenance' });

    if (maintenance)
        return collection.findOneAndUpdate({ name: 'maintenance' }, { $set: { value } });
    else
        return collection.insertOne({ name: 'maintenance', value });
};

const migrate = async () => {
    if (!client) {
        log.error('Can not migrate DB, because not connected to it');
        return false;
    }

    let migrations = await mogront.client.state();

    let migrate = false;

    for (let migration of migrations) {
        let status = migration.status === 'EXECUTED' ? migration.status.green : migration.status.gray;
        console.log(`Migration '${migration.name.yellow}' state: ${status}`);

        if (migration.status === 'PENDING')
            migrate = true;
    }

    if (migrate) {
        console.log('DB Migrations: ' + 'migrating'.yellow);
        await maintenance(true);
        console.log('DB Maintenance mode: ' + 'true'.yellow);

        try {
            let migrated = await mogront.client.migrate();
            for (let migration of migrated)
                console.log(`Migration '${migration.name.yellow}' state: ${migration.status.green}`);

            console.log('DB Migrations: ' + 'migrated'.green);
        }
        catch (e) {
            console.log('DB Migration: ' + 'failure'.red);
            console.log(`Error: ${e.message.red}`);

            return false;
        }
    }
    else {
        console.log('DB Migrations: ' + 'nothing to migrate'.yellow);
    }

    return true;
};

const rollback = async () => {
    if (!client) {
        console.log('Can not rollback last transaction, because not connected to it');
        return false;
    }

    console.log('DB rollback transactions:');

    let rolledback = await mogront.client.rollback();

    for (let roll of rolledback)
        console.log(`Migration '${roll.name.yellow}' rollback`);
};

const connect = async () => {
    try {
        client = await mongodb.MongoClient.connect(
            config.get('db.uri'),
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                poolSize: config.get('db.poolSize') || 10,
                serverSelectionTimeoutMS: config.get('db.connectTimeout') || 30000
            }
        );
    }
    catch (error) {
        client = null;
        console.log('Error connecting to db: ', error);
        return false;
    }

    return true;
};

const disconnect = async () => {
    if (client)
        await client.close();
};

const run = async () => {
    const args = process.argv.slice(2);

    if (args.length === 0) {
        console.log('No arguments. Available commands: migrate, post-migrate, rollback'.red);
        process.exit(1);
        return;
    }

    try {
        let connected = await connect(config.get('db.uri'), config.get('db'));

        if (!connected) {
            console.log('Can not connect to db');
            return false;
        }

        switch (args[0]) {
            case 'migrate':
                mogront.connect(client);

                let migrated = await migrate();
                await disconnect();

                if (!migrated)
                    process.exit(1);

                break;
            case 'post-migrate':

                await maintenance(false);
                await disconnect();

                console.log('DB Maintenance mode: ' + 'false'.green);
                break;
            case 'rollback':
                mogront.connect(client);
                await maintenance(true);
                await rollback();
                await maintenance(false);
                await disconnect();

                break;
            default:
                console.log(`Invalid argument: ${args[0]}. Available commands: migrate, post-migrate`.red);
                process.exit(1);
                break;
        }
    }
    catch (e) {
        console.log(`Error: ${e.message.red}`);
        process.exit(1);
    }

    process.exit(0);
};

run();
