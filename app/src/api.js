import axios from 'axios';
import { router } from './router';
import getApp from './app';

const api = axios.create({
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
});

api.interceptors.response.use(null, error => {
    let status = error.response.status;
    let app = getApp();

    if (status === 401) { // Not authorized
        window.location.replace(`/app/login?redirect=${encodeURIComponent(app.$route.fullPath)}`);
        return;
    }

    if (status === 403) { // Forbidden
        if (error.config.allowForbidden)
            return;

        let token = app.$route.query.token ? `?token=${app.$route.query.token}` : '';

        router.replace(`/forbidden${token}`);
        return;
    }

    if (status === 503) { // Service Unavailable
        router.replace('/unavailable');
        return;
    }

    let messageTitle = 'Неизвестная ошибка';
    let returnPage = false;

    switch (error.response.status) {
        case 400:
            messageTitle = 'Некорректные данные';
            break;
        case 404:
            if (error.config.allowNotFound)
                return;

            messageTitle = 'Не найдено';
            break;
        case 413:
            returnPage = true;
            messageTitle = 'Слишком большой объем данных';
            break;
        case 500:
            returnPage = true;
            messageTitle = 'Внутренняя ошибка сервера';
            break;
    }

    let lineHeight = '';
    let description = error.request.responseURL;

    if (error.response.data.description) {
        description += `<br><div style="white-space: pre">${error.response.data.description}</div>`;
        lineHeight = '30';
    }

    if (returnPage)
        router.go(-1);

    app.$nextTick(() => {
        app.toast.error(description, messageTitle, { position: 'bottomRight', titleLineHeight: lineHeight });
    });

    return Promise.resolve(null);
});

export default api;
