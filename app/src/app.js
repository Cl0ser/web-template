import Vue from 'vue';
import App from './components/App.vue';
import { router } from './router';

let MainApp = null;

const getApp = () => {
    return MainApp;
};

export default getApp;

export const loadApp = () => {
    MainApp = new Vue({
        el: 'app',
        components: { App },
        router
    });
};
