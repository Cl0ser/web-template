import Vue from 'vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import VueIziToast from 'vue-izitoast';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css';
import 'izitoast/dist/css/iziToast.css';

import { loadApp } from './app';

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueIziToast);

Vue.prototype.$defines = require('../../libs/defines');

loadApp();
