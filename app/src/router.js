import Vue from 'vue';
import Router from 'vue-router';

import Index from './components/Index.vue';
import New from './components/New.vue';

Vue.use(Router);

export const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Index',
            component: Index
        },
        {
            path: '/new',
            name: 'New',
            component: New
        }
    ]
});
