'use strict';
const mogront = require('../../mogront');

/**
 * The 'up' method is called on migration.
 *
 * @param {Database} db The MongoClient database instance
 * @returns {Promise}
 */
module.exports.up = async db => {
    const session = mogront.client._mongo.startSession();

    try {
        await session.withTransaction(async () => {
            await db.collection('table').updateMany(
                {},
                {
                    $set: {
                        'show': true,
                    }
                }
            );
        });
    }
    finally {
        await session.endSession();
    }
};

/**
 * The 'down' method is called on rollback.
 *
 * @param {Database} db The MongoClient database instance
 * @returns {Promise}
 */
module.exports.down = async db => {
    const session = mogront.client._mongo.startSession();

    try {
        await session.withTransaction(async () => {
            await db.collection('table').updateMany(
                {},
                {
                    $unset: {
                        'show': '',
                    }
                }
            );
        });
    }
    finally {
        await session.endSession();
    }
};
