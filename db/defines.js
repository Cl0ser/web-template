module.exports.FILE_TYPE = {
    RAW: 'raw',
    ZIP: 'zip',
    TAR: 'tar',
    TAR_GZ: 'tar.gz'
};
