'use strict';

const mongoose = require('mongoose');
const gridFS = require('mongoose-gridfs');

const log = require('../libs/log');
const files = require('./files');

module.exports.connection = null;

module.exports.connect = async (uri, options) => {
    options = options || {};

    mongoose.set('useNewUrlParser', true);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
    mongoose.set('useUnifiedTopology', true);

    try {
        await mongoose.connect(uri, {
            poolSize: options.poolSize || 10,
            serverSelectionTimeoutMS: options.connectTimeout || 30000,
            promiseLibrary: global.Promise
        });

        module.exports.mongoose = mongoose;
        module.exports.connection = mongoose.connection;

        mongoose.connection.on('error', err => {
            log.error('Db error: ', err);
        });

        let filesFS = gridFS.createModel({
            bucketName: 'files',
            modelName: 'files',
            connection: mongoose.connection
        });

        module.exports.filesFS = files.getModelFunctions(filesFS);

        module.exports.defines = require('./defines');
        module.exports.table = require('./table');
    }
    catch (error) {
        log.error('Error connecting to db: ', error);
        return false;
    }

    return true;
};

module.exports.disconnect = async () => {
    if (mongoose.connection)
        await mongoose.disconnect();
};
