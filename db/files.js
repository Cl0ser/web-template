'use strict';

const logger = require('../libs/log');
const s2s = require('string-to-stream');

module.exports.getModelFunctions = model => {
    let functions = {};

    //Old style API for busboy
    functions.pipeStream = (filename, fileStream, meta, mime, callback) => {
        let fileOptions = {
            filename: filename,
            contentType: mime || 'text/plain',
            metadata: meta || {}
        };

        new model(fileOptions).write(fileStream, callback);
    };

    functions.saveStream = (filename, fileStream, meta, mime) => {
        return new Promise((s, r) => {
            let fileOptions = {
                filename: filename,
                contentType: mime || 'text/plain',
                metadata: meta || {}
            };
            new model(fileOptions).write(fileStream, (err, data) => {
                if (err)
                    r(err);
                else
                    s(data);
            });
        });
    };

    functions.save = (filename, fileContent, meta, mime) => {
        return functions.saveStream(filename, s2s(fileContent), meta, mime);
    };

    functions.readStream = (id, stream) => {
        return new Promise((s, r) => {
            let dbStream = model.read({ _id: id });

            dbStream.pipe(stream);

            stream.on('close', () => {
                s();
            });
            stream.on('error', error => {
                r(error);
            });
            dbStream.on('error', error => {
                r(error);
            });
        });
    };

    functions.getStreamWithInfo = async id => {
        let info = await model.findOne({ _id: id });

        if (info) {
            return {
                info,
                stream: model.read({ _id: id })
            };
        }
        else {
            return null;
        }
    };

    functions.read = async id => {
        let file = await functions.readWithInfo(id);

        return file.data;
    };

    functions.readWithInfo = async id => {
        let info = await model.findOne({ _id: id });

        if (!info) {
            logger.warn(`File ${id} not found in GridFs Model: ${model.modelName}`);
            return {
                data: null,
                info: null
            };
        }

        return new Promise((s, r) => {
            model.read({ _id: id }, (err, data) => {
                if (err)
                    r(err);
                else
                    s({
                        data,
                        info
                    });
            });
        });
    };

    functions.fileInfo = id => {
        return model.findOne({ _id: id });
    };

    functions.filesInfo = ids => {
        let promises = ids.map(id => model.findOne({ _id: id }));
        return Promise.all(promises);
    };

    functions.remove = async id => {
        await new Promise((s, r) => {
            model.bucket.deleteFile(id, (err, data) => {
                if (err) {
                    logger.warn(`File ${id} not deleted from GridFs Model: "${model.modelName}" with error: ${err.message}`);
                    s(null);
                }
                else {
                    s(data);
                }
            });
        });
    };

    return functions;
};
