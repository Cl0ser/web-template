'use strict';

const mongoose = require('mongoose');

const TableSchema = new mongoose.Schema({
    iterationName: {
        type: String,
        required: true
    },
    iterationCount: {
        type: Number,
        default: 0
    }
});

TableSchema.index({ iterationName: 1 });

module.exports.schema = TableSchema;
const Table = mongoose.connection.model('Table', TableSchema);
module.exports.model = Table;

module.exports.add = iterationName => {
    return Table.findOneAndUpdate(
        { iterationName },
        {},
        { upsert: true, new: true, setDefaultsOnInsert: true }
    );
};

module.exports.getAll = () => {
    return Table.find({});
};

module.exports.increment = id => {
    return Table.findOneAndUpdate(
        { _id: id },
        {
            $inc: {
                iterationCount: 1
            }
        }
    );
};

module.exports.remove = id => {
    return Table.deleteOne({ _id: id });
};